# Class: printers
#
# This module creates printers from hiera data
#
# Parameters: instance number
#
# Actions:
#
# Requires: see Modulefile
#
# Sample Usage:
#  
#    class {'printers':
#              instance => $instance,
#    }
#

class printers ($instance) {
  if $instance != undef {
    $printers = hiera($instance)
    create_resources( printer, $printers )
  }
  else {
    fail('Error: Custom fact instance not set.')
  }
}
