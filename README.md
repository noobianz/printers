# printers

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with printers](#setup)
    * [What printers affects](#what-printers-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with printers](#beginning-with-printers)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Overview
Manages the configuration of printers using hiera data and a custom fact

## Module Description
Manages the configuration of printers using hiera data and a custom fact


### What printers affects

printers indirectly /etc/cups/printers.conf

### Beginning with printers

	1. Modify/Create hiera.yaml to point at printer data
	2. Create hiera files containg printer data
	3. Test hiera data on puppet master with something like :-

              # hiera -d < instance > ::environment=production

        4. On the clients create a custom fact called instance with a value of 0-9
           
              # mkdir -m 755 -p /etc/facter/facts.d
              # echo 'instance=0' >/etc/facter/facts.d/INSTANCE.txt              

## Usage

 class {'printers':
              instance => $instance,
    }

## Reference

## Limitations

Tested on CentOS / RHEL 6/7

## Development


## Release Notes/Contributors/Etc **Optional**

None
